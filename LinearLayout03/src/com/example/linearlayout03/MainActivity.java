package com.example.linearlayout03;

import android.R.color;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
        
        LinearLayout layout = new LinearLayout(this);
        LinearLayout.LayoutParams param = 
        		new LinearLayout.LayoutParams(
        				LinearLayout.LayoutParams.MATCH_PARENT,
        				LinearLayout.LayoutParams.MATCH_PARENT);
        // 1번 방식, layout과 param 따로 적용
//        setContentView(layout, param);
        //2번 방식, layout에 param 을 포함시켜 적용
        layout.setLayoutParams(param);
        layout.setBackgroundColor(Color.YELLOW);
        
        TextView text = new TextView(this);
        text.setText("글자");
        text.setBackgroundColor(Color.RED);
//        text.setLayoutParams(param);
        layout.addView(text);
        setContentView(layout);
        
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
