package com.example.youtube;

public class ItemVO {
	public String publishedAt;
	public String title;
	public String url;
	public String videoId;
	
	@Override
	public String toString() {
		return "ItemVO [publishedAt=" + publishedAt + ", title=" + title
				+ ", url=" + url + ", videoId=" + videoId + "]";
	}
	public String getPublishedAt() {
		return publishedAt;
	}
	public void setPublishedAt(String publishedAt) {
		this.publishedAt = publishedAt;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getVideoId() {
		return videoId;
	}
	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}
}
