package com.example.youtube;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.youtube.common.UniversalImageLoaderManager;
import com.example.youtube.common.Util;

public class MainActivity extends Activity {

	EditText et;
	String videoId;
	String q, ett;
	String result;
	TextView tv1, tv2, tv3;
	ImageView img;
	RelativeLayout r;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		r = (RelativeLayout) findViewById(R.id.relative);
		tv1 = (TextView) findViewById(R.id.tv1);
		tv2 = (TextView) findViewById(R.id.tv2);
		tv3 = (TextView) findViewById(R.id.tv3);
		img = (ImageView) findViewById(R.id.img);
		et = (EditText) findViewById(R.id.et);

		r.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(MainActivity.this, SubActivity.class);
				intent.putExtra("videoid", videoId);
				// "videoid" 라는 이름으로 videoid 의 값을 넘긴다
				startActivity(intent);
			}
		});

		result = Util.request("https://www.googleapis.com/youtube/v3/search?"
				+ "part=snippet&q=" + "%EC%9B%90%ED%94%BC%EC%8A%A4"
				+ "&key=AIzaSyBxbYr1Y31YM0WOn1qAPtO9OV1t7CzD7Rg&maxResults=50");

		try {
			JSONObject obj = new JSONObject(result);
			JSONArray items = obj.getJSONArray("items");
			ArrayList<ItemVO> list = new ArrayList<ItemVO>();
			for (int i = 0; i < items.length(); i++) {

				JSONObject obj2 = items.getJSONObject(i);
				JSONObject snippet = obj2.getJSONObject("snippet");
				JSONObject thumbnails = snippet.getJSONObject("thumbnails");
				JSONObject def = thumbnails.getJSONObject("medium");
				JSONObject id = obj2.getJSONObject("id");

				String publishedAt = snippet.getString("publishedAt");
				String title = snippet.getString("title");
				String url = def.getString("url");
				videoId = id.getString("videoId");
				
				ItemVO vo = new ItemVO();
				vo.setPublishedAt(publishedAt);
				vo.setTitle(title);
				vo.setUrl(url);
				vo.setVideoId(videoId);
				list.add(vo);
			}
			System.out.println(list);
			
			Toast.makeText(this, result + "", 0).show();

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

}
