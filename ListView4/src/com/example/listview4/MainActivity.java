package com.example.listview4;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private ArrayList<MyItem> items = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		items = new ArrayList<MyItem>();
		items.add(new MyItem(R.drawable.ic_launcher, "노트북"));
		items.add(new MyItem(R.drawable.ic_launcher, "키보드"));
		items.add(new MyItem(R.drawable.ic_launcher, "마우스"));
		
		CustomAdapter adapter = new CustomAdapter(this, R.layout.custom_listview, items);
		
		ListView listView = (ListView) findViewById(R.id.list_view);
		listView.setAdapter(adapter);
	}

}

class MyItem {
	private int icon;
	private String name;
	
	MyItem(int icon, String name) {
		this.icon = icon;
		this.name = name;
	}
	public int getIcon() {
		return icon;
	}
	public String getName() {
		return name;
	}
}

class CustomAdapter extends BaseAdapter {
	private Context context;
	private int layout;
	private ArrayList<MyItem> items;
	private LayoutInflater inflater;
	
	CustomAdapter(Context context, int layout, ArrayList<MyItem> items) {
		this.context = context;
		this.layout = layout;
		this.items = items;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null) {
			convertView = inflater.inflate(layout, parent, false);
		}
		
		ImageView img = (ImageView) convertView.findViewById(R.id.img);
		img.setImageResource(items.get(position).getIcon());
		
		TextView text = (TextView) convertView.findViewById(R.id.text);
		text.setText(items.get(position).getName());
		
		final String name = items.get(position).getName();
		Button btn = (Button) convertView.findViewById(R.id.btn);
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String msg = name + " 주문";
				Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
			}
		});
		
		return convertView;
	}
	
}