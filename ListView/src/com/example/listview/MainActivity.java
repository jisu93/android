package com.example.listview;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//����Ʈ�信�� ����� �� �ִ� ���� : List, �迭
		ArrayList<String> data = new ArrayList<String>();
		data.add("¥���");
		data.add("«��");
		data.add("��ǳ��");
		data.add("������");
		data.add("������");
		data.add("������ä");
		data.add("������");
		data.add("���ĵκ�");
		data.add("������");
		data.add("���");
		data.add("��һ���");
		ListView list = (ListView) findViewById(R.id.list);
		
		//����� �غ�(�������� ��� layout-xml �� �������� ����)
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, data);
		
		//����Ʈ�信 ����� ����
		list.setAdapter(adapter);
		
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) { //arg2 : ������. ���� Ŭ���ƴ���.
				Toast.makeText(MainActivity.this, arg2 + "", 0).show();
			}
		});
	}
}
