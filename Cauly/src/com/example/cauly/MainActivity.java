package com.example.cauly;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
//import android.view.View.OnClickListener;
import android.os.Bundle;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public void onBackPressed() {
		AlertDialog.Builder alert = 
				new AlertDialog.Builder(MainActivity.this);
		alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		    	finish();
		    }
		});
		alert.setNegativeButton("취소", new OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				arg0.dismiss();     //닫기
			}
		});
		alert.setTitle("제목");
		alert.setMessage("테스트 메세지");
		alert.show();
	}
	
	
	
	

}







