package com.example.customlistview;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.sax.StartElementListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CustomAdapter extends BaseAdapter{
	private Context context;
	private int layout;
	private ArrayList<ItemVO> list;
	private LayoutInflater inflater;
	
	public CustomAdapter(Context context, int layout, ArrayList<ItemVO> list) {
		this.context = context;
		this.layout = layout;
		this.list = list;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		if(view == null) {
			view = inflater.inflate(layout, parent, false);
		}
		TextView text1 = (TextView) view.findViewById(R.id.text1);
		TextView text2 = (TextView) view.findViewById(R.id.text2);
		TextView text3 = (TextView) view.findViewById(R.id.text3);
		ImageView img1 = (ImageView) view.findViewById(R.id.img);
		text1.setText(list.get(position).publishedAt.substring(0, 10));
		text2.setText(list.get(position).title);
		text3.setText(list.get(position).description);
		UniversalImageLoaderManager.displayImage(context, list.get(position).getUrl(), img1);
		
//		RelativeLayout click = (RelativeLayout) view.findViewById(R.id.click);
//		click.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View arg0) {
//				Intent intent = new Intent(context, SubActivity.class);
//				intent.putExtra("videoId", list.get(position).videoId);
//				context.startActivity(intent);
//			}
//		});
		return view;
	}

}
