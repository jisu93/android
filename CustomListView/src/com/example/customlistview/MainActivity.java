package com.example.customlistview;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class MainActivity extends Activity {
	ProgressDialog dlg;
	String result;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// StrictMode.ThreadPolicy policy = new
		// StrictMode.ThreadPolicy.Builder()
		// .permitAll().build();
		// StrictMode.setThreadPolicy(policy);

		final ListView view = (ListView) findViewById(R.id.list);
		final ArrayList<ItemVO> list = new ArrayList<ItemVO>();

		dlg = ProgressDialog.show(this, "로딩중", "기다려주세요");
		new Thread(new Runnable() {

			@Override
			public void run() {

				result = Util
						.request("https://www.googleapis.com/youtube/v3/search?"
								+ "part=snippet&q="
								+ "%EC%9B%90%ED%94%BC%EC%8A%A4"
								+ "&key=AIzaSyBxbYr1Y31YM0WOn1qAPtO9OV1t7CzD7Rg&maxResults=50");

				try {
					JSONObject obj = new JSONObject(result);
					JSONArray items = obj.getJSONArray("items");
					for (int i = 0; i < items.length(); i++) {

						JSONObject obj2 = items.getJSONObject(i);
						JSONObject snippet = obj2.getJSONObject("snippet");
						JSONObject thumbnails = snippet
								.getJSONObject("thumbnails");
						JSONObject def = thumbnails.getJSONObject("default");
						JSONObject id = obj2.getJSONObject("id");

						String publishedAt = snippet.getString("publishedAt");
						String title = snippet.getString("title");
						String url = def.getString("url");
						String videoId = "";
						if(!id.isNull("videoId")) {
							videoId = id.getString("videoId");							
						}
						String description = snippet.getString("description");

						ItemVO vo = new ItemVO();
						vo.setPublishedAt(publishedAt);
						vo.setTitle(title);
						vo.setUrl(url);
						vo.setVideoId(videoId);
						vo.setDescription(description);
						list.add(vo);
					}
					System.out.println(list);

					// Toast.makeText(this, result + "", 0).show();

				} catch (JSONException e) {
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						CustomAdapter adapter = new CustomAdapter(MainActivity.this,
								R.layout.listview, list);
						view.setAdapter(adapter);
						try {
							Thread.sleep(3000);
							dlg.dismiss();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				});
			}
		}).start();

		view.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				Intent intent = new Intent(MainActivity.this, SubActivity.class);
				intent.putExtra("videoId", list.get(position).getVideoId());
				startActivity(intent);
			}
		});

	}
}
