package com.example.animal;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends Activity {
	ImageView imgDog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button btnDog = (Button) findViewById(R.id.btn_dog);
		Button btnCat = (Button) findViewById(R.id.btn_cat);
		Button btnRabbit = (Button) findViewById(R.id.btn_rabbit);
		
		imgDog = (ImageView) findViewById(R.id.img_dog);
		final ImageView imgCat = (ImageView) findViewById(R.id.img_cat);
		final ImageView imgRabbit = (ImageView) findViewById(R.id.img_rabbit);
		
		btnDog.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				imgDog.setVisibility(View.VISIBLE);
				imgCat.setVisibility(View.GONE);
				imgRabbit.setVisibility(View.GONE);
			}
		});
		btnCat.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				imgCat.setVisibility(View.VISIBLE);
				imgDog.setVisibility(View.GONE);
				imgRabbit.setVisibility(View.GONE);
			}
		});
		btnRabbit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				imgRabbit.setVisibility(View.VISIBLE);
				imgDog.setVisibility(View.GONE);
				imgCat.setVisibility(View.GONE);
			}
		});
	}

}





