package com.example.listview1;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        String[] dataList = {
    		"열혈강호", "원피스", "나루토", 
    		"블리치", "드래곤볼", "닥터슬럼프", 
    		"진격의거인", "카이지", "헌터x헌터",
    		"베르세르크", "슬램덩크", "용랑전"
        };
        
        ListView listView = (ListView) findViewById(R.id.listview);
        
        ArrayAdapter<String> adapter = 
        		new ArrayAdapter<String>(
        				this, android.R.layout.simple_list_item_1, dataList);
        
        listView.setAdapter(adapter);
        
    }

}