package com.example.linearlayout03self;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class MainActivity extends Activity {

	EditText edit;
	Button butt;
	TextView text;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
        
        LinearLayout linear = new LinearLayout(this);
//        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout.LayoutParams param2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
//        linear.setLayoutParams(param);
        linear.setOrientation(LinearLayout.VERTICAL);
        setContentView(linear);
        
        edit = new EditText(this);
        butt = new Button(this);
        text = new TextView(this);
                
        butt.setBackgroundColor(Color.YELLOW);
        text.setTextColor(Color.BLUE);
        butt.setLayoutParams(param2);
        butt.setText("버튼입니다");
        text.setBackgroundColor(Color.LTGRAY);
        text.setTextSize(30);
//        text.setGravity(Gravity.CENTER);
        text.setPadding(10,0,0,0);
        
        linear.addView(edit);
        linear.addView(butt);
        linear.addView(text);

        butt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				text.setText(edit.getText().toString());
			}
		});
        
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
