package com.example.viewexam;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button butt1 = (Button) findViewById(R.id.b1);
		Button butt2 = (Button) findViewById(R.id.b2);
		final TextView text1 = (TextView) findViewById(R.id.text1);
		final EditText edit1 = (EditText) findViewById(R.id.edit1);
		
		butt1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				text1.setTextColor(Color.YELLOW);
				String t1 = (String) text1.getText();
				text1.setText(t1 + t1);
			}
		});
		butt2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				text1.setTextColor(Color.RED);
				Editable t2 = edit1.getText();
				text1.setText(t2);
			}
		});
	}
}
