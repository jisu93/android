package com.example.pramelayout;

import android.app.Activity;
import android.opengl.Visibility;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;


public class MainActivity extends Activity {

	Button butt1, butt2, butt3;
	LinearLayout red, green, blue;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        butt1 = (Button) findViewById(R.id.butt1);
        butt2 = (Button) findViewById(R.id.butt2);
        butt3 = (Button) findViewById(R.id.butt3);
        red = (LinearLayout) findViewById(R.id.red);
        green = (LinearLayout) findViewById(R.id.green);
        blue = (LinearLayout) findViewById(R.id.blue);
        
        butt1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				red.setVisibility(View.VISIBLE);
				green.setVisibility(View.INVISIBLE);
				blue.setVisibility(View.INVISIBLE);
				
			}
		});
        butt2.setOnClickListener(new OnClickListener() {
        	
        	@Override
        	public void onClick(View arg0) {
        		red.setVisibility(View.INVISIBLE);
        		green.setVisibility(View.VISIBLE);
        		blue.setVisibility(View.INVISIBLE);
        		
        	}
        });
        butt3.setOnClickListener(new OnClickListener() {
        	
        	@Override
        	public void onClick(View arg0) {
        		red.setVisibility(View.INVISIBLE);
        		green.setVisibility(View.INVISIBLE);
        		blue.setVisibility(View.VISIBLE);
        		
        	}
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
