package com.cookandroid.project11_1;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class MainActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setTitle("�׸���� ��ȭ ������");

		final GridView gv = (GridView) findViewById(R.id.gridView1);
		MyGridAdapter gAdapter = new MyGridAdapter(this);
		gv.setAdapter(gAdapter);
	}

	public class MyGridAdapter extends BaseAdapter {
		Context context;

		public MyGridAdapter(Context c) {
			context = c;
		}

		public int getCount() {
			return posterID.length;
		}

		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		Integer[] posterID = { R.drawable.mov01, R.drawable.mov02,
				R.drawable.mov03, R.drawable.mov04, R.drawable.mov05,
				R.drawable.mov06, R.drawable.mov07, R.drawable.mov08,
				R.drawable.mov09, R.drawable.mov10, R.drawable.mov01,
				R.drawable.mov02, R.drawable.mov03, R.drawable.mov04,
				R.drawable.mov05, R.drawable.mov06, R.drawable.mov07,
				R.drawable.mov08, R.drawable.mov09, R.drawable.mov10,
				R.drawable.mov01, R.drawable.mov02, R.drawable.mov03,
				R.drawable.mov04, R.drawable.mov05, R.drawable.mov06,
				R.drawable.mov07, R.drawable.mov08, R.drawable.mov09,
				R.drawable.mov10 };

		public View getView(int position, View convertView, ViewGroup parent) {
			
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.gridview, parent, false);
			
			ImageView img = (ImageView) convertView.findViewById(R.id.img);
			img.setImageResource(posterID[position]);
			
			ImageView imageview = new ImageView(context);
			imageview.setLayoutParams(new GridView.LayoutParams(100, 150));
			imageview.setScaleType(ImageView.ScaleType.FIT_CENTER);
			imageview.setPadding(5, 5, 5, 5);

			imageview.setImageResource(posterID[position]);
			final String[] name = {"���", "�ϵ���", "����", "������Ÿ","���� �Ÿ�",
					"���� ����","���Ϸ���","������ ������","�ﺸ��","������ǻó",
					"���", "�ϵ���", "����", "������Ÿ","���� �Ÿ�",
					"���� ����","���Ϸ���","������ ������","�ﺸ��","������ǻó",
					"���", "�ϵ���", "����", "������Ÿ","���� �Ÿ�",
					"���� ����","���Ϸ���","������ ������","�ﺸ��","������ǻó"};

			final int pos = position;
			
			img.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					View dialogView = (View) View.inflate(MainActivity.this,
							R.layout.dialog, null);
					AlertDialog.Builder dlg = new AlertDialog.Builder(
							MainActivity.this);
					ImageView ivPoster = (ImageView) dialogView
							.findViewById(R.id.ivPoster);
					ivPoster.setImageResource(posterID[pos]);
					dlg.setTitle(name[pos]);
					dlg.setIcon(R.drawable.movies);
					dlg.setView(dialogView);
					dlg.setNegativeButton("�ݱ�", null);
					dlg.show();
				}
			});

			return convertView;
		}

	}
}
