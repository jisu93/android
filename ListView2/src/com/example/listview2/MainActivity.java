package com.example.listview2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        ArrayList<HashMap<String, String>> dataList = 
        		new ArrayList<HashMap<String,String>>();
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("item1", "열혈강호"); map.put("item2", "1번");
        dataList.add(map);
        map = new HashMap<String, String>();
        map.put("item1", "원피스"); map.put("item2", "2번");
        dataList.add(map);
        map = new HashMap<String, String>();
        map.put("item1", "나루토"); map.put("item2", "3번");
        dataList.add(map);
        map = new HashMap<String, String>();
        map.put("item1", "블리치"); map.put("item2", "4번");
        dataList.add(map);
        map = new HashMap<String, String>();
        map.put("item1", "드래곤볼"); map.put("item2", "5번");
        dataList.add(map);
        map = new HashMap<String, String>();
        map.put("item1", "닥터슬럼프"); map.put("item2", "6번");
        dataList.add(map);
        map = new HashMap<String, String>();
        map.put("item1", "진격의거인"); map.put("item2", "7번");
        dataList.add(map);
        map = new HashMap<String, String>();
        map.put("item1", "카이지"); map.put("item2", "8번");
        dataList.add(map);
        map = new HashMap<String, String>();
        map.put("item1", "헌터x헌터"); map.put("item2", "9번");
        dataList.add(map);
        map = new HashMap<String, String>();
        map.put("item1", "베르세르크"); map.put("item2", "10번");
        dataList.add(map);
        map = new HashMap<String, String>();
        map.put("item1", "슬램덩크"); map.put("item2", "11번");
        dataList.add(map);
        map = new HashMap<String, String>();
        map.put("item1", "용랑전"); map.put("item2", "12번");
        dataList.add(map);
        
        
        ListView listView = (ListView) findViewById(R.id.listview);
        
        SimpleAdapter adapter = new SimpleAdapter(
				this, dataList, android.R.layout.simple_list_item_2, 
				new String[] {"item1", "item2"},
				new int[] {android.R.id.text1, android.R.id.text2});
        
        listView.setAdapter(adapter);
    }

}
