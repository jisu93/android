package com.example.toast;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button btn1 = (Button)findViewById(R.id.btn1);
		Button btn2 = (Button)findViewById(R.id.btn2);
		final Button btn3 = (Button)findViewById(R.id.btn3);
		
		btn1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Toast.makeText(
					MainActivity.this, "HI", Toast.LENGTH_SHORT).show();
			}
		});

		btn2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Toast t = Toast.makeText(
						MainActivity.this, 
						"커스텀 출력1", 
						Toast.LENGTH_SHORT);
				t.setGravity(Gravity.TOP, 0, 100);
				t.show();
			}
		});

		btn3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				String s1 = new String("테스트");
				String s2 = btn3.getText().toString();
				
				LinearLayout linear2 = new LinearLayout(getApplicationContext());
				linear2.addView(View.inflate(MainActivity.this, R.layout.toast, null));
				LinearLayout linear = 
						(LinearLayout)View.inflate(
								MainActivity.this, 
								R.layout.toast, 
								null);
				Toast t = Toast.makeText(
						MainActivity.this, 
						"커스텀 출력2", 
						Toast.LENGTH_SHORT);
				t.setGravity(Gravity.CENTER, 0, 0);
				t.setView(linear);
				t.show();
			}
		});
	}

}
