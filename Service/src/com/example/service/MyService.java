package com.example.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

public class MyService extends Service {
	int count = 0;
	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			Log.e("handler", System.currentTimeMillis() + "");
			count++;
			if(count % 10 == 0) {
				Intent intent = new Intent("com.example.service.drawTime");
				intent.putExtra("msg", count + "");
				sendBroadcast(intent);
				Toast.makeText(getApplicationContext(), count + "초 경과", 0).show();
			}
			// 1초 뒤 다시 핸들러 호출
			sendEmptyMessageDelayed(0, 1000);
		}

	};

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.e("onStartCommand", "onStartCommand");
		handler.sendEmptyMessage(0);
		
		IntentFilter filter = new IntentFilter();
		filter.addAction("com.example.service.stop");
		registerReceiver(new BRService(), filter);
		
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {
		Log.e("onCreate", "onCreate");
		super.onCreate();
	}

	@Override
	public void onDestroy() {
		Log.e("onDestroy", "onDestroy");
		super.onDestroy();
	}
	public class BRService extends BroadcastReceiver{

		@Override
		public void onReceive(Context arg0, Intent intent) {
			handler.removeMessages(0);
		}

	}


}