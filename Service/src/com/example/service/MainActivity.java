package com.example.service;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
	Intent intent;
	TextView text;
	Button btn;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		text = (TextView) findViewById(R.id.textView);
		btn = (Button) findViewById(R.id.button);
		
		intent = new Intent(MainActivity.this, MyService.class);
		intent.putExtra("aga", 17);
		startService(intent);
		
		IntentFilter filter = new IntentFilter();
		filter.addAction("com.example.service.drawTime");
		registerReceiver(new BRActivity(), filter);
		
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent brIntent = new Intent("com.example.service.stop");
				sendBroadcast(brIntent);
			}
		});
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
//		stopService(intent);
	}
	
	public class BRActivity  extends BroadcastReceiver{

		@Override
		public void onReceive(Context arg0, Intent intent) {
			String msg = intent.getStringExtra("msg");
			text.setText(msg);
		}

	}

}
