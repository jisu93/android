package com.example.network3;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.network3.common.UniversalImageLoaderManager;
import com.example.network3.common.Util;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		StrictMode.ThreadPolicy policy = 
				new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

		String result = Util.request("http://ggoreb.cafe24.com/data4.jsp");
		
		try {
			JSONObject obj = new JSONObject(result);
			String img = obj.getString("img");
			System.out.println(img);
			Toast.makeText(this, img, 0).show();
			ImageView view = (ImageView) findViewById(R.id.img);
			UniversalImageLoaderManager.displayImage(this, img, view);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
