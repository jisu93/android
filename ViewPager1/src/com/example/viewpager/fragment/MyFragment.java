package com.example.viewpager.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.viewpager.Content;
import com.example.viewpager.R;

public class MyFragment extends Fragment {
	private static final String ARG_SECTION_NUMBER = "section_number";

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static MyFragment newInstance(Content con) {
		MyFragment fragment = new MyFragment();
		Bundle args = new Bundle();
		args.putSerializable("data", con);
		// args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	public MyFragment() {
	}

	// 프래그먼트가 생성되면서 보여지는모습
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_main, container,
				false);
		Bundle args = getArguments(); // 위에서 집어넣은 것을 끄집어내는과정
		Content con = (Content) args.getSerializable("data");

		TextView text = (TextView) rootView.findViewById(R.id.section_label);
		text.setText(con.getTitle());

		ImageView img = (ImageView) rootView.findViewById(R.id.img);
		img.setImageResource(con.getImage());
		
		Button btn = (Button) rootView.findViewById(R.id.btn);
		btn.setText(con.getBtn());
		
		return rootView;
	}

}
