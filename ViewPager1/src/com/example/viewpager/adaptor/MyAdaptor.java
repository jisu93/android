package com.example.viewpager.adaptor;

import java.util.Locale;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

import com.example.viewpager.Content;
import com.example.viewpager.R;
import com.example.viewpager.fragment.MyFragment;

public class MyAdaptor extends FragmentPagerAdapter {
	Content[] cs;
	public MyAdaptor(FragmentManager fm, Content[] cs) {
		super(fm);
		this.cs = cs;
	}
	//보여지는 프래그먼트를 생성
	@Override
	public Fragment getItem(int position) { //count개수만큼 반복
		return MyFragment.newInstance(cs[position]);
	}

	@Override
	public int getCount() {
		// Show 3 total pages.
		return cs.length;
	}

	@Override
	public CharSequence getPageTitle(int position) {
//		Locale l = Locale.getDefault();
//		switch (position) {
//		case 0:
//			return getString(R.string.title_section1).toUpperCase(l);
//		case 1:
//			return getString(R.string.title_section2).toUpperCase(l);
//		case 2:
//			return getString(R.string.title_section3).toUpperCase(l);
//		}
		return null;
	}

}
