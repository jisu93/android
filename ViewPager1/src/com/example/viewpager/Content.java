package com.example.viewpager;

import java.io.Serializable;
	//상속은 2가지, 클래스와 인터페이스
public class Content implements Serializable { //안드로이드에서 보여지는클래스 아님, 자료로만 쓰이는
						//프래그먼트 하나당 보여지는 자료들을 보관?
	String title;
	int image; //안드로이드에서는 이미지가 숫자
	String btn;
	
	public String getBtn() {
		return btn;
	}

	public void setBtn(String btn) {
		this.btn = btn;
	}

	public Content(String title, int image,String btn){
		this.title = title;
		this.image = image;
		this.btn = btn;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getImage() {
		return image;
	}
	public void setImage(int image) {
		this.image = image;
	}
	
	
	
	
}
