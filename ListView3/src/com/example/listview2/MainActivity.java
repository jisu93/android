package com.example.listview2;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private ArrayList<String> list = null;
	private ArrayAdapter<String> adapter = null;
	private ListView listView = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		list = new ArrayList<String>();
		list.add("First");
		list.add("Second");
		list.add("Third");
		
		adapter = 
				new ArrayAdapter<String>(
						this, 
						android.R.layout.simple_list_item_multiple_choice, 
						list);
		
		listView = (ListView) findViewById(R.id.list_view);
		listView.setAdapter(adapter);
		
		listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		listView.setOnItemClickListener(itemCL);
		
		Button addBtn = (Button) findViewById(R.id.add_btn);
		Button removeBtn = (Button) findViewById(R.id.remove_btn);

		addBtn.setOnClickListener(btnCL);
		removeBtn.setOnClickListener(btnCL);
	}
	
	OnClickListener btnCL = new OnClickListener() {
		@Override
		public void onClick(View view) {
			EditText edit = (EditText) findViewById(R.id.edit);
			switch(view.getId()) {
			case R.id.add_btn :
				String text = edit.getText().toString();
				list.add(text);
				adapter.notifyDataSetChanged();
				edit.setText("");
				break;
			case R.id.remove_btn :
				
				SparseBooleanArray spar = listView.getCheckedItemPositions();
				System.out.println(spar.size());
				int a = adapter.getCount();
				for(int i = a-1; i >= 0; i--) {
					if(spar.get(i)) {
						list.remove(i);
						adapter.notifyDataSetChanged();
					}
				}
				
//				int pos = listView.getCheckedItemPosition();
//				list.remove(pos);
				adapter.notifyDataSetChanged();
				break;
			}
		}
	};
	
	OnItemClickListener itemCL = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			String msg = "���� ��� : " + list.get(position);
			Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
		}
	};
}



