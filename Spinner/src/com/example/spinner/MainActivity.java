package com.example.spinner;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setTitle("스피너");
		
		String[] titles = {
			"쿵푸팬더", "짱구는 못말려", "아저씨",
			"아바타", "대부", "국가대표", "토이스토리3",
			"마당을 나온 암탉", "죽은 시인의 사회", "서유기"
		};
		
		final int[] imgs = {
				R.drawable.mov21, R.drawable.mov22,
				R.drawable.mov23, R.drawable.mov24,
				R.drawable.mov25, R.drawable.mov26,
				R.drawable.mov27, R.drawable.mov28,
				R.drawable.mov29, R.drawable.mov30
		};
		
		Spinner spinner = (Spinner) findViewById(R.id.spinner);
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, titles);
		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int pos, long lPos) {
				ImageView img = (ImageView) findViewById(R.id.img);
				img.setImageResource(imgs[pos]);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
	}
}
