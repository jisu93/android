package com.example.viewflipper;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ViewFlipper;

public class MainActivity extends Activity {
	MediaPlayer mp;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button btnPrev = (Button) findViewById(R.id.btn_prev);
		Button btnNext = (Button) findViewById(R.id.btn_next);
		final ViewFlipper flipper = (ViewFlipper) findViewById(R.id.filpper);
		
		mp = MediaPlayer.create(this, R.raw.kalimba);
		mp.start();
		
		btnPrev.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				flipper.showPrevious();
			}
		});
		btnNext.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				flipper.showNext();
			}
		});
	}

	@Override
	protected void onPause() {
		super.onPause();
		mp.pause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		mp.start();
	}
	
}