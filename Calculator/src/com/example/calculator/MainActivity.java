package com.example.calculator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	EditText edit1, edit2;
	Button btnAdd, btnSub, btnMul, btnDiv;
	TextView textResult;
	String num1, num2;
	Integer result;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setTitle("초간단 계산기");
		
		Button btn = (Button) findViewById(R.id.btn);
		btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = 
						new Intent(MainActivity.this, 
								SubActivity.class);
				intent.putExtra("number", R.layout.activity_sub);
				startActivity(intent);
			}
		});

		edit1 = (EditText) findViewById(R.id.Edit1);
		edit2 = (EditText) findViewById(R.id.Edit2);

		btnAdd = (Button) findViewById(R.id.BtnAdd);
		btnSub = (Button) findViewById(R.id.BtnSub);
		btnMul = (Button) findViewById(R.id.BtnMul);
		btnDiv = (Button) findViewById(R.id.BtnDiv);

		textResult = (TextView) findViewById(R.id.TextResult);

		btnAdd.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View arg0, MotionEvent arg1) {
				num1 = edit1.getText().toString();
				num2 = edit2.getText().toString();
				
				// 예외처리 try - catch
				try {
					result = Integer.parseInt(num1) + Integer.parseInt(num2);
					textResult.setText("계산 결과 : " + result.toString());
				} catch(NumberFormatException e) {
					Toast.makeText(MainActivity.this, 
							"숫자만 입력해주세요.", 2000).show();
					edit1.setText("");
					edit2.setText("");
//					edit1.
				}
				
				
				return true;
			}
		});

		btnSub.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View arg0, MotionEvent arg1) {
				num1 = edit1.getText().toString();
				num2 = edit2.getText().toString();
				result = Integer.parseInt(num1) - Integer.parseInt(num2);
				textResult.setText("계산 결과 : " + result.toString());
				return false;
			}
		});

		btnMul.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View arg0, MotionEvent arg1) {
				num1 = edit1.getText().toString();
				num2 = edit2.getText().toString();
				result = Integer.parseInt(num1) * Integer.parseInt(num2);
				textResult.setText("계산 결과 : " + result.toString());
				return false;
			}
		});

		btnDiv.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View arg0, MotionEvent arg1) {
				num1 = edit1.getText().toString();
				num2 = edit2.getText().toString();
				
				if(!num2.equals("0")) {
					float result = 0f;
					result = Float.parseFloat(num1) / Float.parseFloat(num2);
					textResult.setText("계산 결과 : " + result);
				} else {
					// 토스트 메세지 출력
					Toast.makeText(
							MainActivity.this, 
							"0으로 나눌 수 없습니다.", 
							Toast.LENGTH_SHORT).show();
				}
				
//				result = Integer.parseInt(num1) / Integer.parseInt(num2);
				return false;
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater minflater = getMenuInflater();
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if(id == R.id.action_settings) {
			
		}
		return super.onOptionsItemSelected(item);
	}
	

}
